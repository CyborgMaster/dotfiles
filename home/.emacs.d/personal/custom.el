(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fill-column 80)
 '(isearchp-deactivate-region-flag nil)
 '(js2-bounce-indent-p t)
 '(js2-strict-trailing-comma-warning nil)
 '(package-selected-packages
   (quote
    (graphql-mode string-inflection csv-mode toml-mode prelude-rust rust-mode textile-mode sass-mode markdown-mode narrow-indirect isearch+ groovy-mode less-css-mode unfill zop-to-char zenburn-theme yari yaml-mode which-key volatile-highlights vkill undo-tree smartrep smartparens smart-mode-line ruby-tools revive rainbow-mode rainbow-delimiters ov operate-on-number move-text mmm-mode magit json-mode js2-mode inf-ruby imenu-anywhere helm-projectile helm-descbinds helm-ag guru-mode grizzl god-mode gitignore-mode gitconfig-mode git-timemachine gist flycheck expand-region exec-path-from-shell enh-ruby-mode elisp-slime-nav easy-kill discover-my-major diminish diff-hl crux company coffee-mode browse-kill-ring beacon anzu ag ace-window)))
 '(safe-local-variable-values (quote ((local-preserve-whitespace . t))))
 '(tab-width 4))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
